__author__ = 'dasha'
import Cell
import json
import random


class Field:

    width = 30
    height = 20

    def __init__(self):
        self.cells = [[Cell.Cell(x, y, self.generate_random_cell_type_number()) for x in range(self.width)] for y in range(self.height)]
        self.occupied_resource_cells = []

    def generate_random_cell_type_number(self):
        r_i = random.randint(0, 100)
        if r_i < 25:
            return 1
        return 0

    def get_cell_for_json(self, x, y, cell_type=None):
        result = {
            'x': x,
            'y': y,
        }
        if cell_type is None:
            result['type'] = self.cells[y][x].type.get_type_name()
        else:
            result['type'] = cell_type
        if result['type'] == "resource" and not(self.cells[y][x].player is None) and self.cells[y][x].is_base() is False:
            result['player'] = self.cells[y][x].player.color.get_color_number()
        return result

    def occupy_cell(self, x, y, player, base = False):
        self.cells[y][x].set_player(player)
        if self.cells[y][x].type.get_type_name() == "resource":
            self.occupied_resource_cells.append(self.cells[y][x])
        if base is True:
            self.cells[y][x].set_base()

    def get_occupied_resource_cells(self, player):
        result = []
        for y in range(self.height):
            for x in range(self.width):
                if self.cells[y][x].player == player:
                    result.append(self.cells[y][x])
        return result

    def is_cell_occupied(self, x, y):
        return self.cells[y][x].is_occupied()

    def get_json(self, player):
        field_in_format = []
        for y in range(self.height):
            for x in range(self.width):
                cell_to_add = self.get_cell_for_json(x, y)
                field_in_format.append(cell_to_add)
        return field_in_format

    def get_cells_json(self, cells):
        res = []
        for cell in cells:
            res.append(self.get_cell_for_json(x=cell.x, y=cell.y))
        return res

    def is_cell_free(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return False
        cell = self.cells[y][x]
        if cell.is_occupied() is True:
            return False
        return True