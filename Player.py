import random
import struct

__author__ = 'dasha'
import Field


class Player():

    def __init__(self):
        self.x = 0
        self.y = 0
        self.money = 1000
        self.money_increase_speed = 5
        self.color = None
        self.active = False

        self.cells = []
        self.field = None

    def set_color_bag(self, color_bag):
        self.color = color_bag

    def set_field(self, field):
        self.field = field
        self.x = random.randint(0, self.field.width-1)
        self.y = random.randint(0, self.field.height-1)
        self.field.occupy_cell(self.x, self.y, self, True)


    def at_position(self, position):
        if (self.x == position[0]) and (self.y == position[1]):
            return True
        return False

    def get_for_json(self):
        return {
            'position': {
                'x': self.x,
                'y': self.y
             },
            'money': self.money,
            'money_increase_speed': self.money_increase_speed,
            'cells': list(self.cells),
            'color_number': self.color.get_color_number()
        }

    def try_to_kill(self, position):
        x = position['x']
        y = position['y']
        cell = self.field.cells[y][x]
        if self.money < 600:
            return False
        self.money -= 600
        if cell.is_occupied() and cell.type.get_type_name() == "resource":
            cell.player.money_increase_speed -= 10
        cell.kill()
        return True

    def try_to_capture(self, position):
        x = position['x']
        y = position['y']
        can = False
        if self.field.is_cell_occupied(x, y) is True or self.field.cells[y][x].is_killed() is True:
            return False
        for dy in range(3):
            for dx in range(3):
                if not(dy == 1 == dx) and (self.is_element_in_cells({'x': x+1-dx, 'y': y+1-dy}) or self.at_position((x+1-dx, y+1-dy))):
                    can = True
        if can is True:
            if self.field.cells[y][x].type.get_type_name() == "transport":
                if self.money < 100:
                    return False
                self.money -= 100
            elif self.field.cells[y][x].type.get_type_name() == "resource":
                if self.money < 300:
                    return False
                self.money_increase_speed += 10
                self.money -= 300
                can = 'Resource'
            self.add_element_to_cells({'x': x, 'y': y})
            self.field.occupy_cell(x, y, self)
        return can

    def add_element_to_cells(self, position):
        pos = {'x': position['x'], 'y': position['y']}
        self.cells.append(pos)

    def is_element_in_cells(self, position):
        pos = {'x': position['x'], 'y': position['y']}
        return pos in self.cells