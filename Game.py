__author__ = 'dasha'
import Field
import json
import Player
import time
import _thread
from Command import Command
from color.ColorProvider import ColorProvider
from bot.BotContainer import BotContainer


class Game():



    def __init__(self):
        self.field = Field.Field()
        self.users = []
        self.ready = False
        self.finish = False
        self.money_timer = None
        self.bots = []
        _thread.start_new_thread(self.money_increase, ())


    def add_user(self, user, bot=False):
        self.users.append(user)
        player = user.get_player()
        player.set_field(self.field)
        player.set_color_bag(ColorProvider.get_color_set_by_number(len(self.users)-1))
        if bot is False:
            player.active = False
        player.money = 1000
        player.money_increase_speed = 5
        self.send_for_all_users(self.create_users_update_command().get_json())

    def has_user(self, user):
        for usr in self.users:
            if usr.get_email() == user.get_email():
                return True
        return False

    def remove_user(self, user):
        self.users.remove(user)

    def get_users(self):
        return self.users

    def get_users_count(self):
        return len(self.users)

    def send_for_all_users(self, data):
        for user in self.users:
            user.send_message(data)

    def send_all_captured_cells_for_user(self, user):
        data = {}
        for cuser in self.users:
            player = cuser.player
            cells = self.field.get_occupied_resource_cells(player)
            data[player.color.get_color_number] = self.field.get_cells_json(cells)
        user.send_message(Command("update_captured_cells", {"cells":data}).get_json())

    def try_to_start_game(self):
        for user in self.users:
            if user.player.active is False:
                return
        if len(self.users) < 2:
            return
        self.ready = True
        _thread.start_new_thread(self.bot_executor, ())



    def count_of_active_users(self):
        res = 0
        for curr_user in self.users:
            if curr_user.player.active is True:
                res += 1
        return res

    def get_active_users(self):
        res = []
        for curr_user in self.users:
            if curr_user.player.active is True:
                res.append(curr_user)
        return res

    def create_users_update_command(self):
        users_data = []
        for curr_user in self.users:
            users_data.append({'email':curr_user.get_email(), 'active': curr_user.player.active})
        answer_command = Command("load_users", {'users': users_data})
        return answer_command

    def update_users_at_client(self):
        self.send_for_all_users(self.create_users_update_command().get_json())

    def send_winner_information(self):
        active = self.get_active_users()
        winner = None
        if len(active) > 0:
            winner = active[0]
        self.send_for_all_users(Command("winner", winner.get_email()).get_json())

    def attach_bot_to_game(self):
        if len(self.users) > 1:
            return
        bot = BotContainer.get_bot()
        bot.user.player.active = True
        self.add_user(bot.user, True)
        self.bots.append(bot)
        bot.set_game(self)


    def incoming_command(self, user, command):
        if self.finish is True:
            return
        player = user.get_player()
        answer_command = Command("nothing", "to do")
        all = False
        result = False
        if command.is_command("load_field"):
            answer_command = Command("load_field", self.field.get_json(player))
        elif command.is_command("load_player"):
            answer_command = Command("load_player", player.get_for_json())
        elif command.is_command("load_users"):
            answer_command = self.create_users_update_command()
        elif command.is_command("ready"):
            user.player.active = True
            self.try_to_start_game()
            self.update_users_at_client()
        elif command.is_command("add_bot") and self.ready is False:
            self.attach_bot_to_game()
        elif command.is_command("kill") and self.ready is True:
            position = command.get_data()
            x = position['x']
            y = position['y']
            kill_res = player.try_to_kill(position)
            if kill_res is True:
                died = None
                for curr_user in self.users:
                    if curr_user.player.at_position([x, y]) is True:
                        died = curr_user
                        break
                if not(died is None):
                    died.player.active = False
                    died.player.money = 0
                    died.player.money_increase_speed = 0
                    if 0 < self.count_of_active_users() < 2:
                        self.ready = False
                        self.send_winner_information()
                        self.field = Field.Field()
                        for curr_user in self.users:
                            curr_user.player = Player.Player()
                        for curr_bot in self.bots:
                            BotContainer.make_bot_free(curr_bot)
                        self.users = []
                        self.bots = []
                    self.update_users_at_client()
                all = True
                result = True
                answer_command = Command("load_field", self.field.get_cells_json([self.field.cells[y][x]]))
        elif command.is_command("capture_cell") and self.ready is True:
            position = command.get_data()
            capt_res = player.try_to_capture(position)
            x = position['x']
            y = position['y']
            result = True
            if capt_res == 'Resource':
                self.send_for_all_users(Command("update_captured_cells", {"cells": {player.color.get_color_number(): [self.field.get_cell_for_json(x=x, y=y)]}}).get_json())
                answer_command = Command("load_player", { "cells":player.cells, "money":player.money, "money_increase_speed":player.money_increase_speed })
            elif capt_res is True:
                answer_command = Command("load_player", { "cells":player.cells, "money":player.money, "money_increase_speed":player.money_increase_speed })
        if all is True:
            self.send_for_all_users(answer_command.get_json())
        else:
            user.send_message(answer_command.get_json())
        return result

    def money_increase(self):
        while True:
            if self.ready is True:
                for user in self.users:
                    curr_player = user.player
                    curr_player.money += curr_player.money_increase_speed
                    command = Command('load_player', {'money':curr_player.money, "money_increase_speed":curr_player.money_increase_speed})
                    user.send_message(command.get_json())
            time.sleep(1)

    def bot_executor(self):
        while True:
            for bot in self.bots:
                bot.execute()
            time.sleep(3)


