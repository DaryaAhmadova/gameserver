__author__ = 'dasha'
from color.ColorBag import ColorBag


class ColorProvider():

    color_sets = []

    @staticmethod
    def add_color_set(color_bag):
        ColorProvider.color_sets.append(color_bag)

    @staticmethod
    def get_number_of_color_sets():
        return len(ColorProvider.color_sets)

    @staticmethod
    def get_color_set_by_number(number):
        if 0 <= number < ColorProvider.get_number_of_color_sets():
            return ColorProvider.color_sets[number]
        return None