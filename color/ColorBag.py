__author__ = 'dasha'
from color.Color import Color;

# Contains a set of colors for player's objects


class ColorBag():

    def __init__(self):
        self.base = None
        self.transport = None
        self.resource = None
        self.number = 0

    def get_color_number(self):
        return self.number