__author__ = 'dasha'
import yaml
from color.ColorBag import ColorBag
from color.Color import Color
from color.ColorProvider import ColorProvider

class Loader():

    color_config_path = "config/color/colors.yml"

    @staticmethod
    def load():
        Loader.load_colors()

    @staticmethod
    def load_colors():
        stream = open(Loader.color_config_path, 'r')
        conf = yaml.load(stream)
        colors = conf['players_colors']
        color_bag_number = 0
        for color in colors:
            curr_color = colors[color]
            bag = ColorBag()
            bag.base = Color(html_color=curr_color['base'])
            bag.resource = Color(html_color=curr_color['resource'])
            bag.transport = Color(html_color=curr_color['transport'])
            color_bag_number += 1
            bag.number = color_bag_number
            ColorProvider.add_color_set(bag)