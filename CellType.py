__author__ = 'dasha'


class CellType():
    type_number = 0

    def __init__(self, number):
        self.type_number = number

    def get_type_name(self):
        if self.type_number == 0:
            return "transport"
        elif self.type_number == 1:
            return "resource"
        elif self.type_number == 2:
            return "base"
        elif self.type_number == 3:
            return "killed"