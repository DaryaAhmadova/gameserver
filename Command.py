__author__ = 'dasha'

import json

class Command():
    json_of_command = None
    name = None
    data = None

    def __init__(self, command_name="default", command_data="default"):
        self.name = command_name
        self.data = command_data
        self.recalculate_json()

    def recalculate_json(self):
        self.json_of_command = json.dumps({"command": self.name, "data":self.data})

    def get_json(self):
        self.recalculate_json()
        return self.json_of_command

    def load_command(self, json_of_c):
        self.json_of_command = json_of_c
        self.parse_command()

    def parse_command(self):
        try:
            buffer = json.loads(self.json_of_command)
            self.name = buffer['command']
            self.data = buffer['data']
        except ...:
            print("Error in parsing command "+self.json_of_command)

    def is_command(self, command):
        return self.name == command

    def get_data(self):
        return self.data