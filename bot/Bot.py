__author__ = 'dasha'
from Command import Command
import random

class Bot():

    def __init__(self, user):
        self.user = user
        self.user.player.active = True
        self.game = None
        self.way_cost = 0
        self.way = []

    def execute(self):
        x = self.user.player.x
        y = self.user.player.y
        moves = self.get_possible_moves()
        if len(moves) < 1:
            return
        random_resource_cell = self.get_resource_cell(moves)
        if not(random_resource_cell is None):
            self.capture_cell(x=random_resource_cell.x, y=random_resource_cell.y)
            return
        ind = random.randint(0, len(moves)-1)
        self.capture_cell(x=moves[ind].x, y=moves[ind].y)


    def set_game(self, game):
         self.game = game

    def get_possible_moves(self):
        possible_cells = []
        curr_cells = self.user.player.cells.copy()
        field = self.user.player.field
        curr_cells.append({'y':self.user.player.y,'x':self.user.player.x})
        for cell in curr_cells:
            x = cell['x']
            y = cell['y']
            for i in range(3):
                for j in range(3):
                    if i == j == 1:
                        continue
                    if field.is_cell_free(x=x-1+i, y=y-1+j) is True:
                        possible_cells.append(field.cells[y-1+j][x-1+i])
        return possible_cells

    def get_resource_cell(self, cells):
        for cell in cells:
            if cell.type.get_type_name() == "resource":
                return cell
        return None


    def capture_cell(self, x, y):
        command = Command('capture_cell', {'x': x, 'y': y})
        self.game.incoming_command(self.user, command)

    def kill_cell(self, x, y):
        command = Command('kill', {'x': x, 'y': y})
        self.game.incoming_command(self.user, command)