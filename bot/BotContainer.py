__author__ = 'dasha'
from User import User
from bot.Bot import Bot

class BotContainer():

    bots = []
    busy_bots = []

    @staticmethod
    def add_bot():
        new_bot_number = len(BotContainer.bots)
        bot_user = User("bot"+str(new_bot_number))
        bot = Bot(bot_user)
        BotContainer.bots.append(bot)

    @staticmethod
    def get_bot():
        for bot in BotContainer.bots:
            if not(bot.user.email in BotContainer.busy_bots):
                BotContainer.busy_bots.append(bot.user.email)
                return bot
        BotContainer.add_bot()
        return BotContainer.get_bot()

    @staticmethod
    def make_bot_free(bot):
        BotContainer.busy_bots.remove(bot.user.email)