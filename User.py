__author__ = 'dasha'

from UCProvider import UCProvider
import hashlib
from Player import Player


class User():

    def __init__(self, email):
        self.clients = []
        self.email = email
        self.hash = hashlib.sha1(self.email.encode("utf-8")).hexdigest()
        self.player = Player()
        UCProvider.email_to_user[self.email] = self
        UCProvider.hash_to_email[self.hash] = self.email

    def get_player(self):
        return self.player

    def add_client(self, client):
        self.clients.append(client)
        UCProvider.client_to_email[client] = self.email

    def remove_client(self, client):
        if client in self.clients:
            self.clients.remove(client)
        if client in UCProvider.client_to_email.keys():
            UCProvider.client_to_email.pop(client)

    def send_message(self, message):
        for client in self.clients:
            client.write_message(message)


    def get_email(self):
        return self.email

    def get_clients(self):
        return self.clients

    def get_hash(self):
        return self.hash