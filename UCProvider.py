__author__ = 'dasha'


class UCProvider():

    hash_to_email = {}
    email_to_user = {}

    client_to_email = {}

    @staticmethod
    def get_user(client):
        if not (client in UCProvider.client_to_email.keys()):
            return None
        email = UCProvider.client_to_email[client]
        return UCProvider.get_user_by_email(email)

    @staticmethod
    def get_user_by_hash(user_hash):
        if not(user_hash in UCProvider.hash_to_email.keys()):
            return None
        email = UCProvider.hash_to_email[user_hash]
        return UCProvider.get_user_by_email(email)

    @staticmethod
    def get_user_by_email(email):
        if not (email in UCProvider.email_to_user.keys()):
            return None
        return UCProvider.email_to_user[email]

    @staticmethod
    def remove_client(client):
        user = UCProvider.get_user(client)
        if not(user is None):
            user.remove_client(client)