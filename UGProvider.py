__author__ = 'dasha'

from Game import Game


class UGProvider():

    games = []
    max_users_per_game = 2

    @staticmethod
    def init_games(n):
        UGProvider.games = []
        for i in range(n):
            game = Game()
            UGProvider.games.append(game)

    @staticmethod
    def get_places():
        res = []
        for game in UGProvider.games:
            res.append(UGProvider.max_users_per_game - game.get_users_count())
        return res

    @staticmethod
    def get_user_games(user):
        res = []
        for game in UGProvider.games:
            if game.has_user(user):
                res.append(game)
        return res

    @staticmethod
    def attach_user_to_game_by_number(user, game_number):
        if game_number>len(UGProvider.games)-1 or game_number<0:
            return None

        if len(UGProvider.get_user_games(user)) > 0:
            return None

        if UGProvider.games[game_number].get_users_count() < UGProvider.max_users_per_game:
             UGProvider.games[game_number].add_user(user)


    @staticmethod
    def attach_user_to_game(user, game):
        if game.get_users_count() < UGProvider.max_users_per_game:
            game.add_user(user)

    @staticmethod
    def incoming_command(user, command):
        games = UGProvider.get_user_games(user)
        for game in games:
            game.incoming_command(user, command)