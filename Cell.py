__author__ = 'dasha'
import CellType


class Cell():

    def __init__(self, x, y, cell_type):
        self.player = None
        self.base = False
        self.x = x
        self.y = y
        self.type = CellType.CellType(cell_type)

    def set_player(self, player):
        self.player = player

    def is_occupied(self):
        if not(self.player is None):
            return True
        return False

    def set_base(self):
        self.base = True

    def remove_base(self):
        self.base = False

    def is_base(self):
        return self.base

    def kill(self):
        self.type.type_number = 3

    def is_killed(self):
        return self.type.type_number == 3