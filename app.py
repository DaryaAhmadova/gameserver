__author__ = 'dasha'

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.auth
import os
import hashlib
import Game
import Command
from UCProvider import UCProvider
from UGProvider import UGProvider
from User import User
from Loader import Loader
from color.ColorProvider import ColorProvider
from bot.BotContainer import BotContainer

static_path = "web/static/"
templates_path = "web/templates/"

Loader.load()

UGProvider.init_games(10)

def authorize_http(client):
    user_hash = client.get_cookie("user")
    if not(UCProvider.get_user(client) is None):
        return UCProvider.get_user(client)

    if UCProvider.get_user_by_hash(user_hash) is None:
        return None

    user = UCProvider.get_user_by_hash(user_hash)
    #user.add_client(client)

    return user


def authorize_ws(client, user_hash):
    if not(UCProvider.get_user(client) is None):
        return UCProvider.get_user(client)

    if UCProvider.get_user_by_hash(user_hash) is None:
        return None

    user = UCProvider.get_user_by_hash(user_hash)
    user.add_client(client)

    return user


class IndexHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(request):
        user = authorize_http(request)
        if not(user is None):
            if len(UGProvider.get_user_games(user)) == 0:
                return request.redirect("/profile")
            request.render(templates_path+"index.html")
        else:
            return request.redirect("/google")


class ProfileHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(request):
        user = authorize_http(request)
        if not(user is None):
            if request.get_argument('action', True) == "book":
                game_number = int(request.get_argument('room', True))
                UGProvider.attach_user_to_game_by_number(user, game_number)
                return request.redirect("/")
            request.render(templates_path+"profile.html", username=user.get_email(), rooms=UGProvider.get_places())
        else:
            return request.redirect("/google")


class GoogleHandler(tornado.web.RequestHandler, tornado.auth.GoogleMixin):
    @tornado.web.asynchronous
    def get(self):
        if self.get_argument("openid.mode", None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return
        self.authenticate_redirect()

    def _on_auth(self, user):
        if not user:
            self.authenticate_redirect()
            return
        email = user['email']
        if UCProvider.get_user_by_email(email) is None:
            user = User(email)
        else:
            user = UCProvider.get_user_by_email(email)
        user_hash = user.get_hash()
        #user.add_client(self)
        self.set_cookie("user", user_hash)
        self.redirect("/profile")


class WebSocketChatHandler(tornado.websocket.WebSocketHandler):
    # def open(self, *args):
    #     print("connection was opened")

    def on_message(self, message):
        command = Command.Command()
        command.load_command(message)
        if command.is_command("authenticate"):
            user_hash = command.get_data()
            auth_res = authorize_ws(self, user_hash)
            # print (auth_res)

            # if auth_res is None:
            #     self.stream.close()
            #     self.close()
        else:
            user = UCProvider.get_user(self)
            # print(user)
            UGProvider.incoming_command(user, command)


    def on_close(self):
        UCProvider.remove_client(self)


settings = {
    "static_path": os.path.join(os.path.dirname(__file__), static_path),
    "xsrf_cookies": True,
}

app = tornado.web.Application([(r'/chat', WebSocketChatHandler), (r'/profile', ProfileHandler), (r'/', IndexHandler), (r'/google', GoogleHandler)], **settings)
app.listen(80)
tornado.ioloop.IOLoop.instance().start()
